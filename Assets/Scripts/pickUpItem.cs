using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pickUpItem : MonoBehaviour
{
    [SerializeField] private float radius;
    //private bool isHolding = false;
    //private bool isInterestedInLore = false;
    public Camera fpsCamera;
    public GameObject hud;
    public GameObject bioUIElement;
    public GameObject pickupableObject;
    public Text hudInstructionText;
    public Text bioUIText;
    public int objectLayer;
    public LayerMask playerLayer;
    [TextArea(3,10)]
    public string bio;
    // Start is called before the first frame update
    void Start()
    {
        objectLayer = 1 << LayerMask.NameToLayer("Object");
    }

    // Update is called once per frame
    void Update()
    {
        if (Physics.CheckSphere(transform.position, radius, playerLayer))
        {
            Collider[] gsd = Physics.OverlapSphere(transform.position, radius, playerLayer);
            //Physics.Raycast(fpsCamera.transform.position, fpsCamera.transform.forward, out Hitinfo, range);
            ///hud.SetActive(true);
            bioUIElement.SetActive(true);
            bioUIText.text = bio;
            ///if (isHolding == false)
            //{
                //hudInstructionText.text = "Press [E] to pick up \n" + gsd[0].transform.name;
                ///hudInstructionText.text = "Press [E] to interact with the " + gsd[0].GetComponent<DisplayName>().displayName;
            ///}
           // else
          //  {
               // hudInstructionText.text = "Press [E] to drop \n" + gsd[0].GetComponent<DisplayName>().displayName + "\n Press [TAB] to read Item Bio.";

            //}
            ///if (Input.GetKeyDown(KeyCode.E))
            {
               // Debug.Log("hELLO WORLD");
               // isHolding = !isHolding;
              //  if(isHolding == true)
               // {
                 //   SetParent(gsd[0].gameObject);
               // }
               // else
               // {
                    //DetachFromParent(gsd[0].gameObject);
                //}
            }
            //if(Input.GetKeyDown(KeyCode.E))
            {
                //isInterestedInLore = !isInterestedInLore;
               // if(isInterestedInLore)
                {
                   // bioUIElement.SetActive(true);
                    //bioUIText.text = gsd[0].GetComponent<ItemBio>().bio;
                }
                //else
                {
                   // bioUIElement.SetActive(false);
                }
            }
        }
        else
        {
            bioUIElement.SetActive(false);
        }
    }
    //void SetParent(GameObject newParent)
   // {
        //pickupableObject.transform.parent = newParent.transform;
        //newParent.transform.parent = transform;
   // }
   // void DetachFromParent(GameObject newParent)
   // {
        //newParent.transform.parent = null;
    //}
}
