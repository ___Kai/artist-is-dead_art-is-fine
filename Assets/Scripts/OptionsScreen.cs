using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsScreen : MonoBehaviour
{
    public GameObject optionsScreen;
    private bool isWantingToLeave = false;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isWantingToLeave = !isWantingToLeave;
            if (isWantingToLeave)
            {
                Time.timeScale = 0f;
                OpenScreen();
            }
            else
            {
                Time.timeScale = 1f;
                CloseScreen();
            }
        }
    }
    public void OpenScreen()
    {
        optionsScreen.SetActive(true);
    }
    public void CloseScreen()
    {
        optionsScreen.SetActive(false);
    }
}
